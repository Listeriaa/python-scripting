#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import requests
from bs4 import BeautifulSoup
import argparse
from urllib.parse import urljoin

def get_links(url):
    '''
    Function to get all links from the url given
    '''
    html_text = requests.get(url).text
    soup = BeautifulSoup(html_text, 'html.parser')
    links = soup.find_all('a')
    return links


def check_link_status(link, visited_links):
  
    try:
        if link not in visited_links :
            visited_links.add(link)
            response = requests.get(link)

            if response.status_code >= 400 :
                print(f" le lien {link} est cassé")
        
    except Exception as e :
        print(f"{link} : {e}")


def brklnk(url, depth, visited_links):

    if depth >= 0 :
        try :
            check_link_status(url, visited_links)

            if depth >=1 :
                links = get_links(url)
                
                for link in links :
                
                    href = urljoin(url, link.get("href"))
                    
                    brklnk(href, depth-1, visited_links)
     

        except Exception as e :
            print(f"Il y a eu une erreur pour {url} : {e}")
        

        
    

def main() :
    # build an empty parser
    parser = argparse.ArgumentParser()

    # mandatory argument
    parser.add_argument("url", type=str, help="the website you want to go through starting with protocol")

    # optionnal argument with parameter

    parser.add_argument("-d", "--depth", type=int, default=1, help="search depth, (0 to check the url argument)")


    # instruct parser to parse command line arguments
    args = parser.parse_args()
    
    visited_links = set() 
    brklnk(args.url, args.depth, visited_links)

if __name__ == "__main__":
    main()