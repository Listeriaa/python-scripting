#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import argparse
import re


def strextract(directory_path, extension, with_path, all):
    regex = re.compile('\"[^\"]*\"')

    for root, _, filenames in os.walk(directory_path):
        
            for filename in filenames :
                # if all is not selected, excludes hidden files
                if not all and filename.startswith("."):
                    continue

                # is extension is given, file doesn't end with it
                if extension != "" and not filename.endswith(extension):
                    continue

                absolute_path = os.path.join(root, filename)
                
                try :
                    with open(absolute_path) as file :
                        
                        for line in file :
                        
                            result = regex.findall(line)
                            for item in result :
                                item = absolute_path + "  " + item if with_path else item 
                                print(item)
                except Exception as e :
                        print(f"erreur sur le fichier {absolute_path} :", e)
    

def main() :
    # build an empty parser
    parser = argparse.ArgumentParser()

    # mandatory argument
    parser.add_argument("dir", type=str, help="directory to run through")

    # optionnal argument with parameter

    parser.add_argument("-s", "--suffix", type=str, default="", help="limit the search to files with this suffix")

    #optional boolean argument
    parser.add_argument("-p", "--path", action="store_true", help="print the path of file")
    parser.add_argument("-a", "--all", action="store_true", help="includes hidden files starting by . ")

    # instruct parser to parse command line arguments
    args = parser.parse_args()
    
    strextract(args.dir, args.suffix, args.path, args.all)

if __name__ == "__main__":
    main()